Name:           picocli
Version:        4.6.1
Release:        1%{?dist}
Summary:        Java command line parser with both an annotations API and a programmatic API

License:        ASL 2.0
URL:            https://github.com/remkop/picocli

Source0:        https://github.com/remkop/picocli/archive/v%{version}/%{name}-%{version}.tar.gz
Source1:        https://repo1.maven.org/maven2/info/picocli/%{name}/%{version}/%{name}-%{version}.pom

BuildArch:      noarch

BuildRequires:  maven-local

%description
Picocli is a modern library and framework, written in Java, that contains both
an annotations API and a programmatic API. It features usage help with ANSI
colors and styles, TAB auto-completion and nested sub-commands. In a single
file, so you can include it in source form. This lets users run picocli-based
applications without requiring picocli as an external dependency.

%package javadoc
Summary:        API documentation for %{name}

%description javadoc
This package contains the API documentation for %{name}

%prep
%autosetup

# remove bundled jar/class files from source
rm -rf %{_builddir}/%{name}-%{version}/gradle/wrapper/gradle-wrapper.jar

# remove test folder since they require a lot of deps, i.e. spring, asciidoc, etc.
rm -rf %{_builddir}/%{name}-%{version}/src/test

# replace pom.xml with the one in maven repo, since the source is not updated.
cp -p %{SOURCE1} pom.xml

%build
%mvn_build

%install
%mvn_install

%files -f .mfiles
%license LICENSE

%files javadoc -f .mfiles-javadoc
%license LICENSE

%changelog
* Tue Jul 06 2021 Didik Supriadi <didiksupriadi41@gmail.com> - 4.6.1-1
- First picocli package
